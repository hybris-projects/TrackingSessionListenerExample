package servlet;

import javax.servlet.*;
import java.io.IOException;
import java.util.Map;

/**
 * Created by mhaagen on 30.10.2016.
 */
public class HttpFilter implements Filter{

    private FilterConfig filterConfig;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        final Map<String, String[]> parameterMap = servletRequest.getParameterMap();
        if (!parameterMap.isEmpty()){
            TrackingSessionListener.getTrackingService().trace(parameterMap);
            return;
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
        filterConfig = null;
    }
}
