package servlet;

import logging.HybrisCustomerTracingService;
import org.springframework.web.servlet.DispatcherServlet;
import trackingapi.CustomerTrackingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.swing.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mhaagen on 27.10.2016.
 */
@Controller
public class TrackingSessionListener implements HttpSessionListener {
//public class TrackingSessionListener extends DispatcherServlet implements HttpSessionListener {

    //private Logger LOGGER = LoggerFactory.getLogger(TrackingSessionListener.class);
    private static CustomerTrackingService trackingService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String processGetRequests(ModelMap model){
        trackingService.trace(model.keySet().toString());
        return "index";
    }

    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        //LOGGER.error("session created");
        HttpSession session = httpSessionEvent.getSession();
        trackingService = new HybrisCustomerTracingService(session);

        trackingService.trace("Session created.");
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        //LOGGER.error("session destroyed");
        trackingService.trace("Session destroyed.");
    }

    public static CustomerTrackingService getTrackingService() {
        return trackingService;
    }
}
