package logging;

import trackingapi.CustomerTrackingService;

import javax.servlet.http.HttpSession;
import java.util.Date;

/**
 * Created by mhaagen on 28.10.2016.
 */
public class HybrisCustomerTracingService implements CustomerTrackingService {

    private final HttpSession session;

    public HybrisCustomerTracingService(HttpSession session) {
        this.session = session;
    }

    @Override
    public void trace(Object message) {
        System.out.println(String.format("[%s] [%s] - %s", session.getId(), new Date(), message));
    }
}
